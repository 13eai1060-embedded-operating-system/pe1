/*
    FreeRTOS V8.2.1 - Copyright (C) 2015 Real Time Engineers Ltd.
    All rights reserved

    VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation >>!AND MODIFIED BY!<< the FreeRTOS exception.

    >>!   NOTE: The modification to the GPL is included to allow you to     !<<
    >>!   distribute a combined work that includes FreeRTOS without being   !<<
    >>!   obliged to provide the source code for proprietary components     !<<
    >>!   outside of the FreeRTOS kernel.                                   !<<

    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE.  Full license text is available on the following
    link: http://www.freertos.org/a00114.html

    1 tab == 4 spaces!

    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?".  Have you defined configASSERT()?  *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *    FreeRTOS provides completely free yet professionally developed,    *
     *    robust, strictly quality controlled, supported, and cross          *
     *    platform software that is more than just the market leader, it     *
     *    is the industry's de facto standard.                               *
     *                                                                       *
     *    Help yourself get started quickly while simultaneously helping     *
     *    to support the FreeRTOS project by purchasing a FreeRTOS           *
     *    tutorial book, reference manual, or both:                          *
     *    http://www.FreeRTOS.org/Documentation                              *
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *   Investing in training allows your team to be as productive as       *
     *   possible as early as possible, lowering your overall development    *
     *   cost, and enabling you to bring a more robust product to market     *
     *   earlier than would otherwise be possible.  Richard Barry is both    *
     *   the architect and key author of FreeRTOS, and so also the world's   *
     *   leading authority on what is the world's most popular real time     *
     *   kernel for deeply embedded MCU designs.  Obtaining your training    *
     *   from Richard ensures your team will gain directly from his in-depth *
     *   product knowledge and years of usage experience.  Contact Real Time *
     *   Engineers Ltd to enquire about the FreeRTOS Masterclass, presented  *
     *   by Richard Barry:  http://www.FreeRTOS.org/contact
     *                                                                       *
    ***************************************************************************

    ***************************************************************************
     *                                                                       *
     *    You are receiving this top quality software for free.  Please play *
     *    fair and reciprocate by reporting any suspected issues and         *
     *    participating in the community forum:                              *
     *    http://www.FreeRTOS.org/support                                    *
     *                                                                       *
     *    Thank you!                                                         *
     *                                                                       *
    ***************************************************************************

    http://www.FreeRTOS.org - Documentation, books, training, latest versions,
    license and Real Time Engineers Ltd. contact details.

    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool, a DOS
    compatible FAT file system, and our tiny thread aware UDP/IP stack.

    http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
    Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.

    http://www.OpenRTOS.com - Real Time Engineers ltd license FreeRTOS to High
    Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
    licenses offer ticketed support, indemnification and commercial middleware.

    http://www.SafeRTOS.com - High Integrity Systems also provide a safety
    engineered and independently SIL3 certified version for use in safety and
    mission critical applications that require provable dependability.

    1 tab == 4 spaces!
*/

#include <stdlib.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

/* Xilinx includes. */
#include "xil_printf.h"
#include "xgpio.h"
#include "xparameters.h"

#define GPIO_LED_DEVICE_ID  XPAR_GPIO_1_DEVICE_ID
#define GPIO_SW_DEVICE_ID   XPAR_GPIO_0_DEVICE_ID

#define TIMER_ID	1
#define DELAY_10_SECONDS	10000UL
#define DELAY_1_SECOND		100UL
#define TIMER_CHECK_THRESHOLD	9

/*-----------------------------------------------------------*/

/* Read switches & output dice tasks */
static void readSwitches(void *pvParameters);
static void outputLED(void *pvParameters);
u32 encodeLED(u32 sw_value);

/*-----------------------------------------------------------*/
static TaskHandle_t xReadSwitches;
static TaskHandle_t xOutputLED;
static QueueHandle_t xQueue = NULL;

XGpio Gpio_LED; /* The Instance of the GPIO LED Driver */
XGpio Gpio_SW;  /* The Instance of the GPIO switches Driver */

int
main(void)
{
	int err;

	xil_printf("Bryan Jean Tadeus Honof\r\n");

	/* Initialize the GPIO driver */
	err = XGpio_Initialize(&Gpio_LED, GPIO_LED_DEVICE_ID);
	if (err != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Initialize the GPIO driver */
	err = XGpio_Initialize(&Gpio_SW, GPIO_SW_DEVICE_ID);
	if (err != XST_SUCCESS) {
		xil_printf("Gpio Initialization Failed\r\n");
		return XST_FAILURE;
	}

	/* Set the direction as all output */
	XGpio_SetDataDirection(&Gpio_LED, 1, 0x000000);

	/* Set the direction as all input */
	XGpio_SetDataDirection(&Gpio_SW, 1, 0xFFFFFF);

	/* Create the two tasks.  The Tx task is given a lower priority than the
	Rx task, so the Rx task will leave the Blocked state and pre-empt the Tx
	task as soon as the Tx task places an item in the queue. */
	xTaskCreate( 	outputLED,  				/* The function that implements the task. */
					( const char * ) "read",    /* Text name for the task, provided to assist debugging only. */
					configMINIMAL_STACK_SIZE, 	/* The stack allocated to the task. */
					NULL, 						/* The task parameter is not used, so set to NULL. */
					tskIDLE_PRIORITY,			/* The task runs at the idle priority. */
					&xOutputLED );

	xTaskCreate( 	readSwitches, 				/* The function that implements the task. */
					( const char * ) "output", 	/* Text name for the task, provided to assist debugging only. */
					configMINIMAL_STACK_SIZE, 	/* The stack allocated to the task. */
					NULL, 						/* The task parameter is not used, so set to NULL. */
					tskIDLE_PRIORITY,			/* The task runs at the idle priority. */
					&xReadSwitches );

	/* Create the queue used by the tasks.  The Rx task has a higher priority
	than the Tx task, so will preempt the Tx task and remove values from the
	queue as soon as the Tx task writes to the queue - therefore the queue can
	never have more than one item in it. */
	xQueue = xQueueCreate( 	1,						/* There is only one space in the queue. */
							sizeof( u32 ) );	    /* Each space in the queue is large enough to hold a uint32_t. */

	/* Check the queue was created. */
	configASSERT( xQueue );

	/* Start the tasks and timer running. */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	exit(EXIT_FAILURE);
}

/*-----------------------------------------------------------*/
static void
readSwitches(void *pvParameters)
{
	u32 sw_read;

	while (1) {
		/* Delay for 1 second. */
		//vTaskDelay( x1second );

		/* Read the switches current values */
		sw_read = XGpio_DiscreteRead(&Gpio_SW, 1);

		/* Send the next value on the queue.  The queue should always be
		empty at this point so a block time of 0 is used. */
		xQueueSend( xQueue,			       /* The queue being written to. */
					&sw_read,               /* The address of the data being sent. */
					0UL );		           /* The block time. */
	}

	return;
}

/*-----------------------------------------------------------*/
static void
outputLED(void *pvParameters)
{
	u32 sw_value;
	u32 prev_sw_value;

	while (1) {
		prev_sw_value = sw_value;
		/* Block to wait for data arriving on the queue. */
		xQueueReceive( 	xQueue,				/* The queue being read. */
						&sw_value,	        /* Data is read into this address. */
						portMAX_DELAY );	/* Wait without a timeout for data. */

		/* Print the received data. */
		XGpio_DiscreteWrite(&Gpio_LED, 1, encodeLED(sw_value));
		if (prev_sw_value != sw_value)
			xil_printf("sw = 0x%X\r\n", sw_value);
	}

	return;
}

/*-----------------------------------------------------------*/
u32
encodeLED(u32 sw_value)
{
	u32 encoded = 0x00;

	switch (sw_value) {
	case 0x01:
		encoded = 0x10;
		break;
	case 0x02:
		encoded = 0x01 | 0x80;
		break;
	case 0x03:
		encoded = 0x01 | 0x80 | 0x10;
		break;
	case 0x04:
		encoded = 0x20 | 0x80 | 0x01 | 0x04;
		break;
	case 0x05:
		encoded = 0x20 | 0x80 | 0x01 | 0x04 | 0x10;
		break;
	case 0x06:
		encoded = 0x20 | 0x80 | 0x01 | 0x04 | 0x02 | 0x40;
		break;
	default:
		encoded = 0x00;
		break;
	}

	return encoded;
}
